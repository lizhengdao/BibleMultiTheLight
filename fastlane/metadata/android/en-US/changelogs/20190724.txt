3.15 - 20190724


EN:
• Fix: orientation change
• +4 Youtubers



FR:
• Fix: changement d'orientation
• +4 Youtubers



IT:
• Fix: cambio di orientamento
• +4 Youtubers



ES:
• Fix: cambio de orientación
• +4 Youtubers



PT:
• Fix: mudança de orientação
• +4 Youtubers
