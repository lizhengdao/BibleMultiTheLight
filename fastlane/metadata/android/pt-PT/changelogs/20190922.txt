• Você não precisa reiniciar o aplicativo ao alterar algumas preferências
• O idioma selecionado no aplicativo pode ser diferente do idioma do seu sistema
• Ao selecionar uma Bíblia, isso definirá o idioma usado no aplicativo